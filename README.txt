The fixes and information within this repository are intended to help fix
software bugs found in commercial pinball machines.

This repository does not contain the ROM data for games.

Any game patches will need to be applied yourself.  This can be done via a hex editor.
However, in most cases, the software does a checksum (verifies the contents to
an extent) of the data.  Simply applying a patch and saving the file will change the checksum
value, and the ROM will be seen as invalid to the system.  There are checksum tools 
available that you can use, provided you are aware of an area of free space in the ROM where
the bytes can be adjusted in order to correct the checksum.  

Please research your rights regarding patching software.
